package main

import (
	"bytes"
	"codeberg.org/derMicha/goUtils/database"
	"fmt"
	"github.com/gofiber/fiber"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"testing"
)

var (
	testDbName = fmt.Sprintf("%s_test", dbName)
)

func setup() {
	log.Println("test setup")
	database.CleanUpDb(testDbName)
}

func tearDown() {
	log.Println("test teardown")
	database.CloseDatabase()
	database.CleanUpDb(testDbName)
}

func sendReq(t *testing.T, app *fiber.App, req *http.Request, respRegexp []string, testDesc string) {
	res, err := app.Test(req, 100)
	assert.Nilf(t, err, testDesc)

	assert.Equalf(t, 200, res.StatusCode, testDesc)

	body, err := ioutil.ReadAll(res.Body)
	assert.Nilf(t, err, testDesc)
	for _, regExp := range respRegexp {
		re := regexp.MustCompile(regExp)
		match := re.FindStringIndex(string(body))
		assert.Greater(t, len(match), 0)
	}

}

func TestAbout(t *testing.T) {

	testDesc := "about test"
	app := SetupApp(testDbName)
	req, _ := http.NewRequest(
		"GET",
		"/",
		nil,
	)

	sendReq(t, app, req, []string{fmt.Sprintf("%s %s", appName, appVersion)}, testDesc)
}

func TestPostKey(t *testing.T) {
	setup()
	defer tearDown()

	testDesc := "create key test"
	app := SetupApp(testDbName)

	testKey := uuid.New()
	jsonStr := []byte(fmt.Sprintf(`{"key": "%s"}`, testKey))

	req1, _ := http.NewRequest(
		"POST",
		"/api/v1/key",
		bytes.NewBuffer(jsonStr),
	)
	req1.Header.Set("Content-Type", "application/json")
	//sendReq(t, app, req, []string{".*\"key\",.*", fmt.Sprintf(".*\"%s\",.*", testKey)}, testDesc)
	sendReq(t, app, req1, []string{".*\"key\".*", fmt.Sprintf(".*\"%s\".*", testKey)}, testDesc)

	req2, _ := http.NewRequest(
		"GET",
		"/api/v1/key",
		bytes.NewBuffer(jsonStr),
	)
	req2.Header.Set("Content-Type", "application/json")
	sendReq(t, app, req1, []string{".*\"key\".*", fmt.Sprintf(".*\"%s\".*", testKey)}, testDesc)
}
