package main

import (
	"./model/key"
	"codeberg.org/derMicha/goUtils/database"
	"fmt"
	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"log"
)

var (
	appName    = "openC19API"
	appVersion = "v0.0.1"
	dbName     = appName
)

func aboutService(c *fiber.Ctx) {
	c.Send(fmt.Sprintf("%s %s", appName, appVersion))
}

func setupRoutes(app *fiber.App) {
	log.Println("setupRoutes")

	app.Get("/", aboutService)

	apiV1 := app.Group("/api/v1")

	apiV1.Get("/c19key", key.GetKeys, cors.New(cors.Config{AllowMethods: []string{"GET", "HEAD"}, AllowOrigins: []string{"*"}, AllowHeaders: []string{"*/*"}}))
	apiV1.Get("/c19key/:c19key", key.GetKey, cors.New(cors.Config{AllowMethods: []string{"GET", "HEAD"}, AllowOrigins: []string{"*"}, AllowHeaders: []string{"*/*"}}))
	apiV1.Post("/c19key", key.NewKey, cors.New(cors.Config{AllowMethods: []string{"POST", "HEAD"}, AllowOrigins: []string{"*"}, AllowHeaders: []string{"*/*"}}))

}

func SetupApp(databaseName string) *fiber.App {
	log.Println("SetupApp")
	app := fiber.New()

	database.InitDatabase(databaseName)
	database.MigrateDatabase(&key.C19Key{})

	setupRoutes(app)

	return app
}

func main() {
	log.Println("Welcome to openC19Tracker")

	app := SetupApp(dbName)
	defer database.CloseDatabase()

	err := app.Listen(3000)
	if err != nil {
		log.Fatal(fmt.Sprintf("server could not be started: %s", err.Error()))
	}
}
