package key

import (
	"codeberg.org/derMicha/goUtils/database"
	"fmt"
	"github.com/gofiber/fiber"
	"time"
)

type C19Key struct {
	ID        int64     `json:"id" gorm:"column:id" gorm:"primaryKey"`
	C19Key    string    `json:"c19_key" gorm:"uniqueIndex"`
	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
}

func NewKey(c *fiber.Ctx) {
	db := database.GetDb()
	key := new(C19Key)
	if err := c.BodyParser(key); err != nil {
		c.Status(503).Send(err)
		return
	}

	if key.CreatedAt.IsZero() {
		key.CreatedAt = time.Now()
	}
	if key.C19Key != "" {
		db.Create(&key)
		err := c.JSON(key)
		if err != nil {
			err := fiber.NewError(500, fmt.Sprintf("key points to invalid json dcumenet: '%s'", key.C19Key))
			c.Next(err)
		}
	} else {
		err := fiber.NewError(406, fmt.Sprintf("no valid key: '%s'", key.C19Key))
		c.Next(err)
	}

	if key.ID == 0 {
		err := fiber.NewError(406, fmt.Sprintf("dublicate key: '%s'", key.C19Key))
		c.Next(err)
	}
}

func GetKey(c *fiber.Ctx) {
	c19key := c.Params("c19key")
	db := database.GetDb()

	var key C19Key
	db.First(&key, C19Key{C19Key: c19key})

	if key.ID == 0 {
		err := fiber.NewError(404, fmt.Sprintf("key could not be found: '%s'", c19key))
		c.Next(err)
	} else {
		err := c.JSON(key)
		if err != nil {
			err := fiber.NewError(500, fmt.Sprintf("key points to invalid json dcumenet: '%s'", key.C19Key))
			c.Next(err)
		}
	}
}

func GetKeys(c *fiber.Ctx) {
	db := database.GetDb()
	var keys []C19Key
	db.Find(&keys)
	err := c.JSON(keys)
	if err != nil {
		err := fiber.NewError(500, "invalid json dcuments")
		c.Next(err)
	}
}
